from jira import JIRA
import os, sys

class Jira:

    def __init__(self, ticket):

        if 'ATLASSIAN_API_TOKEN' not in os.environ:
            print('missing gitlab API token, please add environment variable "export ATLASSIAN_API_TOKEN=<key>" and run this script again')
            sys.exit(1)
        
        self.jira = self.getJIRA()
        self.ticket = self.jira.issue(ticket)
    
    def getJIRA(self):
        username, apiToken = os.environ['ATLASSIAN_API_TOKEN'].split(':')
        return JIRA(server=os.environ['ATTLASIAN_SERVER'], basic_auth=(username, apiToken))

    def getFunctionizeTests(self):
        functionizeTests = []
        for subtask in self.ticket.fields.subtasks:
            subtask = self.jira.issue(subtask.key)
            for url in subtask.raw['fields']['customfield_17550'].split('\n'):
                ft = url.replace('[','').replace(']','').replace('*','').split('|')[0].replace('https://app.functionize.com/test/','').strip()
                functionizeTests.append(ft)
        return functionizeTests

    def updateTicket (self, result):        
        comment = '||Test name||Result||\n' if result['TESTPASSED'] else '||Test name||Result||Failed Step||Failed Reason||\n'
        for test in result['RESULTSET'][0]['tests']:
            if result['TESTPASSED']:
                comment += '|' + test['name'] + '|' + test['status'] + '|\n'
            else:
                if test['status'] == 'Failure':
                    comment += '|' + test['name'] + '|' + test['status'] + '|' + test['failedStep'] + '|' + test['failedReason'] + '|\n'
                else:
                    comment += '|' + test['name'] + '|' + test['status'] + '| | |\n'

        self.jira.add_comment(self.ticket, comment)