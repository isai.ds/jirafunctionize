import requests, sys, os, json, time

class Functionize:
    URL_ENDPOINT = 'https://app.functionize.com/api/v4'

    def __init__(self, functionizeTests):        
        if 'FUNCTIONIZE_CLIENT_ID' not in os.environ:
            print('missing Functionize Client ID, please add environment variable "export FUNCTIONIZE_CLIENT_ID=<key>" and run this script again')
            sys.exit(1)
        if 'FUNCTIONIZE_CLIENT_SECRET' not in os.environ:
            print('missing Functionize Client Secret, please add environment variable "export FUNCTIONIZE_CLIENT_SECRET=<key>" and run this script again')
            sys.exit(1)
        self.functionizeTests = functionizeTests
    
    def run (self):
        isSuccess = True
        self.generateToken()
        runId = self.runFunctionizeTests()         
        
        while True:
            job = self.getStatus(runId)
            print(job)
            if job['STATUS'] == 'SUCCESS' and job['MESSAGE'] == 'Run status.' and job['RESULTSET'][0]['Status'] == 'Completed':
                break
            time.sleep(20)            
        
        if job['RESULTSET'][0]['failure'] > 0:
            isSuccess = False            

        result = self.getResult(runId)
        result['TESTPASSED'] = isSuccess
        return result
    
    def getResult(self, runId):
        url = Functionize.URL_ENDPOINT + '/tests/run/' + runId + '/result'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'accesstoken': self.token
        }
        body = {
            'run_id': runId,
            'response_type': 'json'
        }

        response = requests.get(url, headers=headers, params=body)
        if response.status_code != 200:
            print('Failed getResults Functionize API call')
        
        return response.json()
    def getStatus(self, runId):
        url = Functionize.URL_ENDPOINT + '/tests/run/' + runId + '/status'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'accesstoken': self.token
        }
        body = {
            'run_id': runId,
            'response_type': 'json'
        }

        response = requests.get(url, headers=headers, params=body)
        if response.status_code != 200:
            print('Failed getStatus Functionize API call')
        
        return response.json()

    def runFunctionizeTests(self):        
        url = Functionize.URL_ENDPOINT + '/tests/run'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'accesstoken': self.token
        }
        body = {
            'test_ids': ','.join(self.functionizeTests),
            'response_type': 'json'
        }

        response = requests.post(url, headers=headers, params=body)
        runId = response.json()['RESULTSET']['run_id']
        print('runId: ', runId)
        return runId
        

    def generateToken(self):
        url = Functionize.URL_ENDPOINT + '/generateToken'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        params = {
            'apikey': os.environ['FUNCTIONIZE_CLIENT_ID'],
            'secret': os.environ['FUNCTIONIZE_CLIENT_SECRET'],
            'response_type': 'json'
        }
        req = requests.PreparedRequest()
        req.prepare_url(url, params)
        response = requests.post(req.url, headers=headers)
        self.token = response.json()[0]['access_token']
        print('token: ', self.token)
    
