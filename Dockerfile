FROM ubuntu:focal as base

ENV DEBIAN_FRONTEND=noninteractive

# Install packages
RUN <<EOF
apt-get update
apt-get install -y --no-install-recommends python3.9 python3.9-dev python3-pip build-essential 
rm -rf /var/lib/apt/lists/*
EOF

# Configuring python
RUN mkdir -p /usr/src/app
COPY requirements.txt /usr/src/app
RUN pip3 install --no-cache-dir -r /usr/src/app/requirements.txt
COPY . /usr/src/app

RUN <<EOF
chmod +x /usr/src/app/jira-functionize.sh
EOF

ENV JIRA_FUNCTIONIZE_SCRIPT_PATH=/usr/src/app
ENV PATH=$PATH:/usr/src/app/