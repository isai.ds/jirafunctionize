import argparse
import os, sys

from src import _jira, _functionize

parser = argparse.ArgumentParser(description='Run functionize testing in pipelines')
parser.add_argument('--ticket', type=str, help='Jira issue ticket number', required=True)

args = parser.parse_args()

jira = _jira.Jira(args.ticket)

funtionizeTests = jira.getFunctionizeTests()
print('Tests to run',funtionizeTests)

if funtionizeTests:
    functionize = _functionize.Functionize(funtionizeTests)
    result = functionize.run()
    jira.updateTicket(result)

    if not result['TESTPASSED']:
        print('Functionize tests failed')
        sys.exit(1)